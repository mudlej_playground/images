## Screenshots
| Home                                                               | Home                                                                   |
| :-:                                                                | :-:                                                                    |
| <img src ="home_non_periodic_group.png" width="190" height="400"/> | <img src ="home_80_days_periodic_group.png" width="190" height="400"/> |


| Add Item                                            | Add Item                                                 |
| :-:                                                 | :-:                                                      |
| <img src ="add_item.png" width="190" height="400"/> | <img src ="add_item_more.png" width="190" height="400"/> |

| Add Item                                                 | Groups                                                   |
| :-:                                                      | :-:                                                      |
| <img src ="add_item_type.png" width="190" height="400"/> | <img src ="groups.png" width="190" height="400"/>        |
